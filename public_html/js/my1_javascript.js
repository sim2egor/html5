/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

      /* global google */

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);
    var dd= [
         ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
         ['2004/05',  165,      938,         522,             998,           450,      614.6],
         ['2005/06',  135,      1120,        599,             1268,          288,      682],
         ['2006/07',  157,      1167,        587,             807,           397,      623],
         ['2007/08',  139,      1110,        615,             968,           215,      609.4],
         ['2008/09',  136,      691,         629,             1026,          366,      569.6]
      ];

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable(dd);

    var options = {
      title : 'Monthly Coffee Production by Country',
      vAxis: {title: 'Cups'},
      hAxis: {title: 'Month'},
      seriesType: 'bars',
      series: {5: {type: 'line'}}
    };

    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
//         основная функция расчёта
  function bolivia(b,i)
  {
              // <b> некий параметр обозначающий мощь гос-ва <i> время в месяцах
      return b+b*(Math.exp(1-1/i));
  }
    
function refresh1(){
    var b=700;
    var i=1;
    var dd= [
         ['Месяц', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Порог вхождения'],
         ['2105/05',  bolivia(b,i++),      bolivia(938,i),      bolivia(938,i),             bolivia(998,i),           bolivia(450,i),      2800],
         ['2105/06',  bolivia(b,i++),      bolivia(938,i),      bolivia(938,i),             bolivia(998,i),           bolivia(450,i),      2900],
         ['2105/07',  bolivia(b,i++),      bolivia(938,i),      bolivia(938,i),             bolivia(998,i),           bolivia(450,i),      3000],
         ['2105/08',  bolivia(b,i++),      bolivia(938,i),      bolivia(938,i),             bolivia(998,i),           bolivia(450,i),      3500],
         ['2105/09',  bolivia(b,i++),      bolivia(938,i),      bolivia(938,i),             bolivia(998,i),           bolivia(450,i),      4000]
      ];
        var data = google.visualization.arrayToDataTable(dd);
    var options = {
      title : 'Monthly nuclear production by Country',
      vAxis: {title: 'Kilo'},
      hAxis: {title: 'Month'},
      seriesType: 'bars',
      series: {5: {type: 'line'}}
    };

    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
    chart.draw(data, options);
      
}